// Extends the Driver and add Commands
import { elementWaitTimeOut } from './timeouts';
import { WebDriver, WebElement, WebElementPromise, until, Locator, Key, ILocation, By } from 'selenium-webdriver';

export class SeleniumWebDriverWrapper {

    constructor(private readonly _driver: WebDriver){}

    waitUntilElementLoadedAndDisplayed = async (locator: Locator): Promise<WebElementPromise> => {
        // Create string del locator
        const locatorString = JSON.stringify(locator);

        // Search element by locator
        this._driver.wait(until.elementsLocated(locator), elementWaitTimeOut, `ERROR: Element not located: ${locatorString}`);

        // Check element is visible
        this._driver.wait(until.elementIsVisible(this._driver.findElement(locator)), elementWaitTimeOut, `ERROR: Element is not visible: ${locatorString}`);

        return this._driver.findElement(locator);

    }

    waitUntilPageElementsAreLoadedAndDisplayed = async (locators: Array<Locator>): Promise<any> => {
        let elements = locators.map(async locator => await this.waitUntilElementLoadedAndDisplayed(locator));
        return (Promise as any).all(elements);
    }

    waitUntilCondition = (condition: any) => {
        this._driver.wait(condition);
    }

    waitUntilElementIsEnabled = async (locator: Locator): Promise<any> => {
        // Create string del locator
        const locatorString = JSON.stringify(locator);
        return this._driver.wait(until.elementIsEnabled(this._driver.findElement(locator)), elementWaitTimeOut, `ERROR: Element is not enabled: ${locatorString}`)
    } 

    waitUntilElementIsDisabled = async (locator: Locator): Promise<any> => {
        // Create string del locator
        const locatorString = JSON.stringify(locator);
        return this._driver.wait(until.elementIsDisabled(this._driver.findElement(locator)), elementWaitTimeOut, `ERROR: Element is enabled: ${locatorString}`)
    }

    setWindowSize = async (height: number, width: number) => {
        await this._driver.manage().window().setSize(width, height);
    }

    maximizeWindowSize = async () => {
        await this._driver.manage().window().maximize();
    }

    minimizeWindowSize = async () => {
        await this._driver.manage().window().minimize();
    }

    findElement = async (locator: Locator): Promise<WebElementPromise> =>  this._driver.findElement(locator);
    
    findElements = async (locator: Locator): Promise<WebElement[]> =>  this._driver.findElements(locator);

    getURL = async (url:string): Promise<void> => await this._driver.get(url);

    click = async (locator: Locator): Promise<void> => await (await this.findElement(locator)).click();

    submitForm = async (locator: Locator): Promise<void> => await (await this.findElement(locator)).submit();

    typeText = async (locator: Locator, text: string): Promise<void> => await (await this.findElement(locator)).sendKeys(text);

    getText = async (locator: Locator): Promise<string> => await (await this.findElement(locator)).getText();

    getAttribute = async (locator: Locator, attribute: string): Promise<string> => await (await this.findElement(locator)).getAttribute(attribute);
    
    clearFormField = async (locator: Locator) => {
        const element = await this.findElement(locator);
        if(await element.getTagName() === 'input'){
            const text = await element.getAttribute('value');
            for (let index = 0; index < text.length; index++) {
                await element.sendKeys(Key.BACK_SPACE);
            }
        }
    }

    clearFormValues = async (locator: Locator) => (await this.findElements(locator)).forEach(el => {
            el.click();
            el.clear();
        });

    mouseOverAndClick = async (locator: Locator) => {
        const element = await this.findElement(locator);
        await this._driver.actions().mouseMove(element).click().perform();
    }

    mouseLeave = async (locator: Locator) => {
        const element = await this.findElement(locator);
        const elementLocation = await element.getLocation();
        const newLocation: ILocation = {
            x: elementLocation.x + 1,
            y: elementLocation.y + 1,
        }
        await this._driver.actions().mouseMove(elementLocation, newLocation).perform();
    }     

    navigateForward = async (): Promise<void> => await this._driver.navigate().forward();

    navigateBackwards = async (): Promise<void> => await this._driver.navigate().back();

    navigateToURL = async (url: string): Promise<void> => await this._driver.navigate().to(url);

    refreshPage = async (): Promise<void> => await this._driver.navigate().refresh();

    executeJS = async (script: string, element: WebElement) => await this._driver.executeScript(script, element); 

    scrollToElement = async (element: WebElement) => {
        const elementLocation = await element.getLocation();
        await this._driver.actions().move(elementLocation).perform();
    }
    
    // TODO: More Commands for our personalized Selenium Driver
}


