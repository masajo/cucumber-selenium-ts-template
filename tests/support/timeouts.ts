import { setDefaultTimeout } from '@cucumber/cucumber';

export const elementWaitTimeOut = 60000;
export const stepImplementation = 120000;

// Default TimeOut
const defaultTimeOut = 4000;
setDefaultTimeout(defaultTimeOut);
