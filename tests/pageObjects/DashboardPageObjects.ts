import { WebDriver, By, ByHash, WebElement } from 'selenium-webdriver';
import { SeleniumWebDriverWrapper } from '../support/seleniumWebDriverWrapper';

export class DashboardPageObjects extends SeleniumWebDriverWrapper{

    constructor(_driver: WebDriver){
        super(_driver)
    }

    getWelcomeMessage = async (): Promise<any> => await this.waitUntilElementLoadedAndDisplayed(By.id('message'));

}





