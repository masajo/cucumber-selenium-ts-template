import { WebDriver, By, ByHash } from 'selenium-webdriver';
import { SeleniumWebDriverWrapper } from '../support/seleniumWebDriverWrapper';

export class LoginPageObjects extends SeleniumWebDriverWrapper{

    usernameElementField: ByHash = {
        id: 'txtUsername',
        tagName: 'input'
    }

    passwordElementField: ByHash = {
        id: 'txtPassword',
        tagName: 'input'
    }

    constructor(_driver: WebDriver){
        super(_driver)
    }

    fillUsername = async (text: string) => {
        this.typeText(By.id(''), text);
    }

    clearUsername = async () => {
        this.clearFormField(By.id(''))
    }

    fillPassword = async (text:string) => {
        this.typeText(By.id(''), text);
    }

    clearPassword = async () => {
        this.clearFormField(By.id(''))
    }

    submit = async () => {
        this.submitForm(By.id(''));
    }

}





