export interface Config {
    capabilites: {
        browser: 'chrome' | 'firefox' | 'edge' | 'android' | 'ios' | 'safari';
        browserName: 'chrome' | 'firefox' | 'edge' | 'android' | 'ios' | 'safari';
        project?: string;
        build?: string;
        os?:string;
        server?:string;
        chromeOptions?: object;
        failure_url?: string;
        device?: string;
        resolution?: string
        // TODO: More Capabilities for Driver
    }
}