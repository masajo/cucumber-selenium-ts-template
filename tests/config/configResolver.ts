import { Config } from "./config";
import { configName } from '../support/env';
import { headless } from "./configTypes";

export const customConfig = () => {

    switch(configName.toLowerCase()){
        case 'headless':
            return headless
        // TODO: More options to Custom the device, os, browser, etc.
    }


}