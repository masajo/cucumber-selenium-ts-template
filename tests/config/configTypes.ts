import { Config } from "./config";
import {  } from '../support/env';


// Default Config
const commonConfig: Config = {
    capabilites: {
        browser: 'chrome',
        browserName: "chrome",
        failure_url: undefined,
        // More Default configs
    }
}


// Resolution Config
const defaultViewPortResolution = {
    capabilites: {
        resolution: '1000x1000'
    }
}

// Chrome


// Firefox


// Edge


// Android & devices


// iOS & devices


// Headless
const headlessConfig: Config = {
    capabilites: {
        browser: "chrome",
        browserName: "chrome",
        chromeOptions: {
            args: [
                '--headless'
            ]
        }
    }
}




export const headless = [
    headlessConfig
]






