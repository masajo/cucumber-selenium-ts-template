import { Given, When, Then, Before } from '@cucumber/cucumber';
import { By, WebDriver, WebElement } from 'selenium-webdriver';
import { DashboardPageObjects } from '../pageObjects/DashboardPageObjects';
import { LoginPageObjects } from '../pageObjects/LoginPageObjects';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../support/constants';
import { SeleniumWebDriverWrapper } from '../support/seleniumWebDriverWrapper';
import { stepImplementation } from '../support/timeouts';


// BEFORE EACH
let driver: WebDriver;
let driverWrapper: SeleniumWebDriverWrapper = new SeleniumWebDriverWrapper(driver);
let loginPageObjects: LoginPageObjects;
let dashboardPageObjects: DashboardPageObjects;

// Set Window Size
driverWrapper.setWindowSize(SCREEN_HEIGHT, SCREEN_WIDTH);
driverWrapper.maximizeWindowSize();
// Navigate to URl
driverWrapper.navigateToURL('');

// GIVEN
Given(/^The user is in login page$/, async () => {
    loginPageObjects.navigateToURL('');
})

// WHEN
When(/^The user fills & submits loginForm$/, async () => {
    await loginPageObjects.fillUsername('Admin');
    await loginPageObjects.fillPassword('admin123');
    await loginPageObjects.submit();
});

// THEN
Then(/^The welcome message is shown$/, async () => {
    let message: WebElement = await dashboardPageObjects.getWelcomeMessage();
    // expect(message).toBeDefined();
});


